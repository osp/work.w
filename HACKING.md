```bash
sudo apt-get install postgresql
sudo apt-get install postgresql-server-dev-9.4
sudo apt update
sudo apt install python3-dev
git clone git@gitlab.constantvzw.org:osp/work.w.git
cd work.w
virtualenv --python=python3 venv
source venv/bin/activate
pip install -r requirements/base.txt
cp organon/local_settings.example.py organon/local_settings.py
createuser --interactive
createdb [name]
    Then edit local_settings.py with 
        'NAME': '[name]',
        'USER': '[name]',
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver
npm install -save postcss postcss-cssnext postcss-cli postcss-import
```

organon1969

see https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-16-04

