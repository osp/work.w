<https://organon.pw/>

Organon is a free/libre software that allows on to create scores to describe any performance, action, activity or process.
 
Organon uses *Notation W*, a system that operates on tasks or instructions set out in common language and organized according to a rigorous syntax.
 
Organon makes it possible to encode an existing performance for conservation or transmission purposes. It is also a composition tool for choreographers, directors, performers - and anyone who wants to organize or model an action.
 
Organon is developed by W.