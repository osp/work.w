#! /usr/bin/env bash

echo "var lang = {"

cat playground/templates/playground/underscore/*.mtpl | \
tr "<" "\n" | \
    grep "t(" | \
    cut -d "'" -f 2 | \
    sort | \
    uniq | \
    sort | \
    xargs -0 | while read -r line ; do
        echo "   \"${line}\": \"\"",
    done
echo "}"
