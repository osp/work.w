from django.contrib import admin
from adminsortable2.admin import SortableAdminMixin, SortableInlineAdminMixin
from .models import Attachment, Score, FeaturedScore


class FeaturedScoreAdmin(SortableAdminMixin, admin.ModelAdmin):
    # list_display = ('__unicode__', 'web_publish_date', 'visible')
    pass


class AttachmentAdmin(admin.ModelAdmin):
    pass


class ScoreAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        # obj.user = request.user
        super(ScoreAdmin, self).save_model(request, obj, form, change)


admin.site.register(FeaturedScore, FeaturedScoreAdmin)
admin.site.register(Attachment, AttachmentAdmin)
admin.site.register(Score, ScoreAdmin)


from django.contrib.flatpages.admin import FlatPageAdmin
from django.contrib.flatpages.models import FlatPage
from django.db import models

from ckeditor.widgets import CKEditorWidget

class FlatPageCustom(FlatPageAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget}
    }

admin.site.unregister(FlatPage)
admin.site.register(FlatPage, FlatPageCustom)
