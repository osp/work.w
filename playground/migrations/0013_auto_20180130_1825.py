# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-01-30 18:25
from __future__ import unicode_literals

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('playground', '0012_auto_20180130_1215'),
    ]

    operations = [
        migrations.AddField(
            model_name='score',
            name='language',
            field=models.TextField(blank=True),
        ),
    ]
