from django.db import models
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import JSONField
from taggit.managers import TaggableManager



class Attachment(models.Model):
    title = models.CharField(max_length=255)
    attachment = models.FileField(blank=True, null=True)
    url = models.URLField(blank=True)


class Score(models.Model):
    """docstring"""

    SCORE_TYPE_CHOICES = (
        (0, "inconnu"),
        (1, "transcription"),
        (2, "prescription"),
        (3, "traduction")
    )

    title = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    score_type = models.PositiveSmallIntegerField(
        default=0, choices=SCORE_TYPE_CHOICES)
    score_author = models.TextField(blank=True)
    performance_author = models.TextField(blank=True)
    presentation = models.TextField(blank=True)
    effectif = JSONField(blank=True)
    stage_set = models.TextField(blank=True)
    duration = models.TextField(blank=True)
    genre = models.TextField(blank=True)
    tags = TaggableManager(blank=True)
    mainline = JSONField(blank=True)
    language = models.TextField(blank=True)

    is_public = models.BooleanField(default=False)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, blank=True, null=True, related_name="creator")
    shared_with = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True)

    def __str__(self):
        return self.title

    # def save(self, *args, **kwargs):
    #     import ipdb; ipdb.set_trace()
    #     super(Score, self).save(*args, **kwargs) # Call the "real" save() method.

    def get_absolute_url(self):
        return "/partitions/{}".format(self.id)

    class Meta:
        permissions = (
            ('view_score', 'Can view score'),
        )
        ordering = ['title']


class FeaturedScore(models.Model):
    score = models.ForeignKey(Score)
    order = models.PositiveSmallIntegerField("ordre", default=0, blank=False, null=False)

    class Meta:
        ordering = ("order",)

    def __str__(self):
        return self.score.title
