from django.contrib.auth.models import User
from .models import Attachment, Score
from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField
from taggit_serializer.serializers import (TagListSerializerField,
                                           TaggitSerializer)
### from guardian.shortcuts import assign_perm, get_users_with_perms, get_user_perms, remove_perm
from django.contrib.auth.models import User
from django.contrib.flatpages.models import FlatPage

from django.contrib.auth.validators import UnicodeUsernameValidator



class FlatPageSerializer(serializers.HyperlinkedModelSerializer):
    # id = serializers.ReadOnlyField()

    class Meta:
        model = FlatPage
        fields = ['url', 'title', 'content']


class UserSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = User
        fields = ['username', 'id']
        # Dealing with "A user with that username already exists." Thanks!
        # https://medium.com/django-rest-framework/dealing-with-unique-constraints-in-nested-serializers-dade33b831d9
        extra_kwargs = {
            'username': {
                'validators': [UnicodeUsernameValidator()],
            }
        }


class AttachmentSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = Attachment
        fields = '__all__'
        # read_only_fields = ('attachment',)


class ScoreSerializer(TaggitSerializer, serializers.HyperlinkedModelSerializer):
    id = serializers.ReadOnlyField()
    tags = TagListSerializerField(required=False)
    is_editable = serializers.SerializerMethodField()
    created_by = UserSerializer(required=False)
    shared_with = UserSerializer(many=True, required=False)

    class Meta:
        model = Score
        fields = '__all__'

    def create(self, validated_data):
        validated_data["created_by"] = self.context['request'].user
        # created_by = validated_data.pop('created_by')
        shared_with = validated_data.pop('shared_with')
        instance = Score.objects.create(**validated_data)
        return instance

    def get_is_editable(self, obj):
        current_user = self.context['request'].user
        return obj.created_by == current_user or current_user in obj.shared_with.all()

    def update(self, instance, validated_data):
        # Dealing with "A user with that username already exists." Thanks!
        # https://medium.com/django-rest-framework/dealing-with-unique-constraints-in-nested-serializers-dade33b831d9
        created_by_data = validated_data.pop('created_by')

        shared_with = validated_data.pop('shared_with')
        self.instance.shared_with.clear()
        for i in shared_with:
            user = User.objects.get(username=i["username"])
            if user:
                print("adding {}".format(user.username))
                self.instance.shared_with.add(user)
        instance = super(ScoreSerializer, self).update(instance, validated_data)

        return instance 


class ScoreLightSerializer(serializers.HyperlinkedModelSerializer):
    """A serializer exposing just a subsets of field when we don't need the full
    representation.
    """
    id = serializers.ReadOnlyField()
    tags = TagListSerializerField()

    class Meta:
        model = Score
        fields = ('id', 'title', 'tags', 'language', 'created_at', 'updated_at', 'score_author')
