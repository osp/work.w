;
window.W = window.W || {};


;
(function(undefined) {
    'use strict';

    W.ScoreApp = Marionette.Application.extend({
        region: 'body',

        triggers: {
            keydown: 'keydown'
        },

        channelName:'user',

        onStart: function(options) {
            var channel = this.getChannel();
            var radio = this.getChannel();

            this.model = new W.UserAuthModel();
            // this.listenTo(this.model, 'change', function() { radio.trigger('change', this.model); });
            this.listenTo(this.model, 'change', function() { 
                console.log('chanded');
                // var view = this.getView();
                // var main = view.getRegion('main');
                // main.currentView.render();
                // console.log("rendered");
                //
                Backbone.history.loadUrl();
            });

            var baseView = new W.BaseView({model: this.model});
            this.showView(baseView);
            baseView.model.fetch();
            
            // hide modals when typing escape
            $(document).on("keydown", function(event) {
                if (event.keyCode == 27) {
                    baseView.hideModal();
                }
            })

            var router = new W.ScoreRouter({application: this});

            $(document).on("click", "a", function(e) {
                // Do not interfere with Backbone Paginator.
                if (! e.isDefaultPrevented() && ! W.utils.is_external_link(e.target)) {
                    e.preventDefault(); // This is important

                    var href = $(e.currentTarget).attr('href');

                    router.navigate(href, true);
                };
            });

            /** Starts the URL handling framework */
            Backbone.history.start({ pushState: true });
        }
    });
})();
