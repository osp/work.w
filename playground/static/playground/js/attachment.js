

// Might have a model? A model means saved to the server?
// Otherwise a more simple object
// How to differentiate between synced model and file? Marker?
W.AttachmentField = Backbone.Marionette.CollectionView.extend({
    childView: W.AttachmentField,

    ui: {
        'title': 'input[name="title"]',
        'file': 'input[name="file"]'
    },
    
    events: {
        'click button': 'delete'
    },

    template: '#field-attachment-template',

    delete: function (e) {
        e.preventDefault();
        var r = confirm("Vous êtes sur le point de supprimer ce pièces-jointes. Continuer?");

        if (r) {
            this.model = null;
            this.render();
        }
    },

    value: function () {
        if (this.model) {
            return this.model;
        } else {
            return {
                title: this.ui.title.val(),
                file: this.ui.file.get(0).files[0]
            }
        }
    }
});

(function () {
    var model = {}, callback;
  
    removeQueue.add((function remove (accept, reject) {
      model.destroy({
        success: function () {
          if (callback) {
            callback();
          }
          accept();
        },
        error: reject
      })
    }));
  })()
  
  
  (function () {
    var model = {}, callback;
    uploadQueue.add((function upload (accept, reject) {
      var formData = new FormData();
      formData.append('title', entry.title);
      formData.append('attachment', entry.file);
  
      $.ajax({
          type: "POST",
          url: '/api/attachments/',
          enctype: 'multipart/form-data',
          data: formData,
          cache: false,
          contentType: false,
          processData: false,
          success: _.bind(function (data) {
              model.set(data);
              if (callback) {
                callback();
              }
              accept();
          }, this),
          error: reject,
          dataType: 'json'
      });
    }));
  }) ();
  