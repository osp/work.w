;
window.W = window.W || {};


(function (undefined) {
    'use strict';


    W.ToggableBehavior = Marionette.Behavior.extend({
        ui: {
            'toggle': '.panel__toggle',
        },

        triggers: {
            'click @ui.toggle': 'toggle',
        },

        onShow: function () {
            this.$el.removeClass('is-collapsed');
        },

        onHide: function () {
            // console.log("hiding");
            this.$el.addClass('is-collapsed');
        },

        onToggle: function () {
            this.$el.toggleClass('is-collapsed');
        },
    });


    W.ModalBehavior = Marionette.Behavior.extend({
        ui: {
            'close': '.btn-close',
            'submit': '.btn-submit',
            'register': '.js-register',
        },

        triggers: {
            'click @ui.close': 'hide:modal',
            'click @ui.submit': 'submit',
            'click @ui.register': 'show:register',
        },
    });
})();
