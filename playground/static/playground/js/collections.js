;
window.W = window.W || {};


;
(function (undefined) {
    'use strict';

    W.ChoiceCollection = Backbone.Collection.extend({
        initialize: function(models, options) {
            this.action = options.action;
            this.url = '/api/scores/' + this.action + '/';

            var that = this;

            this.listenTo(options.syncWith, 'request', function(collection, xhr, options) {
                that.fetch({data: options.data})
            });
        },

        model: W.ChoiceModel,
    });


    W.UserCollection = Backbone.Collection.extend({
        url: '/api/users/',

        model: W.UserModel
    });

    W.ScoreCollection = Backbone.PageableCollection.extend({
        url: '/api/scores/',

        model: W.ScoreModel,

        // see <http://borodaalex.blogspot.be/2016/02/pagination-and-filtering-with.html>
        parseState: function (resp, queryParams, state, options) {
            return {totalRecords: resp.count};
        },

        parseRecords: function (resp, options) {
            if (resp.results) {
                return resp.results;
            } else {
                return resp;
            }
        },

        queryParams: {
            currentPage: "page",
            pageSize: "page_size",
            totalRecords: "count",
            // See <https://github.com/backbone-paginator/backbone.paginator/issues/264>
            order: null,
            sortKey: "ordering",
            ordering: function () {
                var sortKey = this.state.sortKey, order = this.state.order;
                if (sortKey && order !== 0) {
                    return (order === 1 ? '-' : '') + sortKey;
                }
                return null;
            }
        }
    });


    W.EntryCollection = Backbone.Collection.extend({
        model: W.EntryModel
    });


    W.PermissionCollection = Backbone.Collection.extend({
        model: W.PermissionModel
    });
})();
