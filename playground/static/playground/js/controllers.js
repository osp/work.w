;
window.W = window.W || {};


;
(function (undefined) {
    'use strict';

    W.ScoreController = Marionette.MnObject.extend({
        home: function () {
            document.title = 'Organon';

            var baseView = this.getOption('application').getView();
            var view = new W.HomeView();
            baseView.showChildView('main', view);
            
            baseView.getChildView('help').triggerMethod("hide");
        },

        
        about: function () {
            document.title = 'Organon: La notation W';

            var baseView = this.getOption('application').getView();
            var view = new W.AboutView();
            baseView.showChildView('main', view);
            baseView.getChildView('help').triggerMethod("hide");
        },

        
        credits: function () {
            document.title = 'Organon: Crédits';

            var baseView = this.getOption('application').getView();
            var view = new W.CreditsView();
            baseView.showChildView('main', view);
            baseView.getChildView('help').triggerMethod("hide");
        },

        
        userList: function () {
            document.title = 'Organon: liste des utilisateurs';

            var baseView = this.getOption('application').getView();
            var view = new W.UserListView();
            baseView.showChildView('main', view);
            
            baseView.getChildView('help').triggerMethod("hide");
        },

        
        userDetail: function () {
            document.title = 'Organon: liste des partitions de l\'utilisateur';

            var baseView = this.getOption('application').getView();
            var view = new W.UserDetailView();
            baseView.showChildView('main', view);
            
            baseView.getChildView('help').triggerMethod("hide");
        },


        scoreList: function () {
            document.title = 'Organon: liste des partitions';

            var baseView = this.getOption('application').getView();
            var view = new W.ScoreListView();
            baseView.showChildView('main', view);

            baseView.getChildView('help').triggerMethod("hide");
            // baseView.getChildView('header').triggerMethod("show");
        },
        
        
        scoreDetail: function (id) {
            document.title = 'Organon: partition';

            var baseView = this.getOption('application').getView();

            // Backbone relational refuse to instantiate more than one model
            // with the same id
            var scoreModel = W.ScoreModel.find({ id: id }) || new W.ScoreModel({ id: id });

            var myScoreDetailView = new W.ScoreDetailView({ model: scoreModel });

            // Attach the views when the model is successfully fetched
            scoreModel.fetch({
                success: (function () {
                    baseView.showChildView('main', myScoreDetailView);
                }).bind(this),
                error: function () {
                    baseView.showChildView('main', new W.ForbiddenScoreDetailView());
                }
            });

            baseView.getChildView('header').triggerMethod("hide");
        }
    });
})();
