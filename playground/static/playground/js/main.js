;
window.W = window.W || {};


;
(function(undefined) {
    'use strict';

    Marionette.TemplateCache.prototype.compileTemplate = function compileTemplate(rawTemplate, options) {
        return W.extendedTemplate(rawTemplate, options);
    }

    Marionette.setRenderer(Marionette.TemplateCache.render);


    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!W.utils.csrfSafeMethod(settings.type) && !this.crossDomain) {
                var csrftoken = W.utils.getCookie('csrftoken');
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });


    // Declares the namespace to Django Relational
    Backbone.Relational.store.addModelScope(W);


    W.config = W.config || {};
    W.config.lang = W.utils.getUserLanguage();

    var scoreApp = new W.ScoreApp();
    scoreApp.start();
})();
