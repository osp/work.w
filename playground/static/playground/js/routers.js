;
window.W = window.W || {};


;
(function (undefined) {
    'use strict';

    W.ScoreRouter = Marionette.AppRouter.extend({
        appRoutes: {
            '': 'home',
            'la-notation-w(/)': 'about',
            'credits(/)': 'credits',
            'partitions(/)': 'scoreList',
            'partitions/:id(/)': 'scoreDetail',
            'users(/)': 'userList',
            'compte(/)': 'userDetail',
        },

        initialize: function (options) {
            this.controller = new W.ScoreController(options);
        },
    });
})();
