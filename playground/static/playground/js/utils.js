;
window.W = window.W || {};
window.W.utils = window.W.utils || {};


(function (undefined) {
    'use strict';

    W.utils.is_external_link = function (el) {
        // data-external is used for attachments
        return (el.host !== window.location.host) || ('external' in el.dataset);
    };


    W.utils.animatedScroll = function (el, distance, duration) {
        var start = Date.now(),
            step = Math.max(distance / (duration / 60), 3),
            tick = function () {
              if (Date.now() - start > duration) {
                // Animation is taking longer than requested
                // scroll the remaining distance at onces
                el.scrollBy(0, distance);
              } else if (distance > 0) {
                el.scrollBy(0, step);
                distance -= step;
                window.requestAnimationFrame(tick)
              }
            };
      
        window.requestAnimationFrame(tick);
    };


    W.utils.getUserLanguage = function () {
        var lang = W.utils.getCookie("lang");

        if (!lang) {
            lang = window.navigator.userLanguage || window.navigator.language;
            lang = lang.substring(0,2)
        }

        return lang;
    }


    W.utils.translate = window.t = function (string) {
        if (!(W.config.lang in W.lang)) {
            // console.log(W.config.lang + " n'est pas une langue defini. On passe au français");
            W.config.lang = "fr"
        };

        var entries = W.lang[W.config.lang];

        if (string in entries) {
            return entries[string];
        } else {
            console.log(string + " n'est pas traduit");
            return string;
        }
    }


    W.utils.setCookie = function (key, value) {
        var expires = new Date();
        expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
        document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
    }


    W.utils.getCookie = function (name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }


    W.utils.csrfSafeMethod = function (method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
})();

