from django.conf.urls import url

from playground.views import ScoreView


urlpatterns = [
    url(r'^', ScoreView.as_view(), name='score'),
]
