module.exports = ctx => ({
  map: ctx.options.map,
  plugins: {
    'postcss-cssnext': {
      features: {
        customProperties: false
      }
    },
    'postcss-import': {},
    'postcss-selector-matches': {},
  }
})
