var Axis = Backbone.Model.extend({
    defaults: {
        id:1,
        titre:'Sans titre', //(uniq)
        contingent:false,
        module_:false,
        aspect:'', //choix dans un array (duratif, itératif, sémelfactif)
        indications:'',
        piece_jointe:'', //url qui pointe vers un document
        terme:'', //(str)
        boucle:'', //de n à p
        alternative:'', //choix dans un array (exclusive, inclusive, conditionnelle) Validation importante des conditions dans une boucle inclusive
        condition:'', //(voir validation ci-dessus)
        tag:'', //Validation: peut avoir un tag seulement si l'axe n'est pas principal, de plus le tag doit être le même pour ses siblings. Succession ordonnée, sans ordre, simultaméité, accumulation
        goto:null //reference de l'axe vers lequel il renvoie
    },
    validate: function (attrs){
	if ( attrs.id > 3 ){
	    return 'Mauvais id.';
	}
    },
    explain: function(){
	return this.get('titre')  +  ' est un axe.';}
});

var AxisCollection= Backbone.Collection.extend({
	model: Axis
});

var AxisView = Backbone.View.extend({
    tagName: 'li',
    className:'axis',
    template: _.template( $('#axisTemplate').html()),

/*    initialize: function(){
	this.render();
    },
*/

    render: function(){
	this.$el.html( this.template(this.model.toJSON()));
	return this;
    }
});

var AxisCollectionView = Backbone.View.extend({
    tagName: 'ul',

    render: function(){
	console.log("entering render");
	console.log(this.collection);
	this.collection.each(function(axis){
	    var axisView = new AxisView({ model: axis });
	    //get children
	    this.$el.append(axisView.render().el); // adding all the person objects.
	}, this);
	return this;
    }
});

var axiscoll= new AxisCollection([
    {titre:"Premier axe", children:[{titre:'sous axe'},{titre:'autre sous axe'}]},
    {titre:"Deuxième axe"}
]);
var aView=new AxisCollectionView({collection:axiscoll});
var ar=aView.render();
$(document.body).append(ar.el);