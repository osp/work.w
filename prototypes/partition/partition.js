function Partition() {
    this.titre = '';
    this.presentation = "";
    this.mise='';
    this.notes='';
    this.auteur='';
    this.date_creation='';
    this.date_modification='';
    this.axe=null;

}
 
Partition.prototype.getInfo = function() {
    var str_ret='<ul>';
    str_ret+='<li>'+this.titre+'</li>';
    str_ret+='<li>'+this.presentation+'</li>';
    str_ret+='<li>'+this.mise+'</li>';
    str_ret+='<li>'+this.notes+'</li>';
    str_ret+='<li>'+this.auteur+'</li>';
    str_ret+='<li>'+this.date_creation+'</li>';
    str_ret+='<li>'+this.date_modification+'</li>';
    str_ret+='</ul>';
    return str_ret;
};

function Axe(){
      this.id='';
      this.titre=''; //(uniq)
      this.contingent=false;
      this.module=false;
      this.aspect=''; //choix dans un array (duratif, itératif, sémelfactif)
      this.indications=''; 
      this.piece_jointe=''; //url qui pointe vers un document
      this.terme=''; //(str)
      this.boucle=''; //de n à p
      this.alternative=''; //choix dans un array (exclusive, inclusive, conditionnelle) Validation importante des conditions dans une boucle inclusive
      this.condition=''; //(voir validation ci-dessus)
      this.tag=''; //Validation: peut avoir un tag seulement si l'axe n'est pas principal, de plus le tag doit être le même pour ses siblings. Succession ordonnée, sans ordre, simultaméité, accumulation
      this.goto=null; //reference de l'axe vers lequel il renvoie
}
