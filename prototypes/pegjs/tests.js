var PEG = require('pegjs');
var assert = require('assert');
var fs = require('fs'); // for loading files

// Read file contents
var grammar = fs.readFileSync('w.pegjs', 'utf-8');
var data = fs.readFileSync('input.w', 'utf-8');

// Create my parser
var parse = PEG.generate(grammar).parse;

// Do a test
// assert.deepEqual( parse("hello"), ["ehllo"] );
// assert.deepEqual( parse(data), ["ehllo"] );
console.log( parse(data) );
