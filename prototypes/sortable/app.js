// https://github.com/ilikenwf/nestedSortable


$(document).ready(function() {
    $('.sortable').nestedSortable({
        handle: 'div',
        items: 'li',
        toleranceElement: '> div',
        cancel: "[contenteditable]"
        protectRoot: true,
        rootID: 'foo',
        isAllowed: function(placeholder, placeholderParent, currentItem) {
            return true;
        }
    });

    $('.add').on("click", function(){
    })
});
